package ictgradschool.industry.lab08.ex02;

import ictgradschool.Keyboard;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class MyReader {

    public void start() {

        // TODO Prompt the user for a file name, then read and print out all the text in that file.
        // TODO Use a BufferedReader.
        System.out.print("Please enter file name: ");
        String fileName = Keyboard.readInput();
        try (BufferedReader bR = new BufferedReader(new FileReader(fileName))){
            String line ="";
            while((line = bR.readLine())!=null){
                System.out.println(line);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static void main(String[] args) {
        new MyReader().start();
    }
}

package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieReader;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieReader extends MovieReader {

    @Override
    protected Movie[] loadMovies(String fileName) {

        // TODO Implement this with a PrintWriter
        ArrayList<Movie> filmsData = new ArrayList<Movie>();
        try (Scanner scanner = new Scanner(new FileReader(fileName))) {
            scanner.useDelimiter(",|\\r\\n");
            while (scanner.hasNextLine()) {
                String name = scanner.next();
                System.out.println("name: "+name);
                int year = scanner.nextInt();
                System.out.println("year: "+year);
                int time = scanner.nextInt();
                System.out.println("time: "+time);
                String director = scanner.next();
                System.out.println("director: "+director+"/n");
                filmsData.add(new Movie(name, year, time, director));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        Movie[] films = new Movie[filmsData.size()];
        for (int i = 0; i < filmsData.size(); i++) {
            films[i] = filmsData.get(i);
        }

        System.out.println("Movies loaded successfully from " + fileName + "!");
        return films;
    }

    public static void main(String[] args) {
        new Ex4MovieReader().start();
    }
}

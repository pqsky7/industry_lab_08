package ictgradschool.industry.lab08.ex04;

import ictgradschool.industry.lab08.ex03.Movie;
import ictgradschool.industry.lab08.ex03.MovieWriter;

import java.io.*;

/**
 * Created by anhyd on 20/03/2017.
 */
public class Ex4MovieWriter extends MovieWriter {

    @Override
    protected void saveMovies(String fileName, Movie[] films) {

        // TODO Implement this with a Scanner
        try(PrintWriter pW = new PrintWriter(new FileWriter(fileName))) {
            for (int i =0; i < films.length;i++) {
                String line=films[i].getName()+","+films[i].getYear()+","+films[i].getLengthInMinutes()+","+films[i].getDirector();
                if(i+1==films.length){
                    pW.print(line);
                }else {
                    pW.println(line);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Movies saved successfully to " + fileName + "!");
    }

    public static void main(String[] args) {
        new Ex4MovieWriter().start();
    }

}

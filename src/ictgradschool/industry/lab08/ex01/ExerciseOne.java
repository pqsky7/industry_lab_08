package ictgradschool.industry.lab08.ex01;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class ExerciseOne {

    public void start() {

        printNumEsWithFileReader();

        printNumEsWithBufferedReader();

    }

    private void printNumEsWithFileReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a FileReader.
        int num = 0;
        try(FileReader fR = new FileReader("input2.txt")){

            while((num = fR.read())!=-1){
                total++;
                if((char)num == 'e' || (char) num =='E'){
                    numE++;
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error: file not found");
        } catch (IOException e) {
            System.out.println("IO Error");
        }
        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    private void printNumEsWithBufferedReader() {

        int numE = 0;
        int total = 0;

        // TODO Read input2.txt and print the total number of characters, and the number of e and E characters.
        // TODO Use a BufferedReader.
        int num = 0;
        try(BufferedReader bR = new BufferedReader(new FileReader("input2.txt"))){
            while((num = bR.read())!=-1){
                total++;
                if((char)num == 'e' || (char) num =='E'){
                    numE++;
                }
            }
        } catch (FileNotFoundException e) {
            System.out.println("Error: file not found");
        } catch (IOException e) {
            System.out.println("IO Error");
        }

        System.out.println("Number of e/E's: " + numE + " out of " + total);
    }

    public static void main(String[] args) {
        new ExerciseOne().start();
    }

}
